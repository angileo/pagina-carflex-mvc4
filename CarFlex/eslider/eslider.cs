﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarFlex.eslider
{
    public class eslider
    {
        public string Url { get; set; }
        public string Texto1 { get; set; }
        public string Texto2 { get; set; }
        public string Texto3 { get; set; }
        public string Texto4 { get; set; }
        public string Img1 { get; set; }
        public string Img2 { get; set; }
        public string ImgFondo { get; set; }

         public eslider()
        {
        }
         public eslider(string url, string t1, string t2, string t3, string t4, string im1, string im2, string imf)
        {
            this.Url = url;this.Texto1 = t1;this.Texto2 = t2;this.Texto3 = t3;this.Texto4 = t4;this.Img1 = im1;this.Img2 = im2;this.ImgFondo = imf;
            
        }
    }
}
