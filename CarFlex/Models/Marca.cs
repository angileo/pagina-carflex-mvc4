﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarFlex.Models
{
    public class Marca
    {
        public string LocCode { get; set; }
        public int MarcaId { get; set; }
        public string MarcaName { get; set; }

        public static IQueryable<Marca> GetMarcas()
        {
            return new List<Marca>
            {
                new Marca {
                    LocCode = "AC",
                    MarcaId = 1,
                    MarcaName = "TOYOTA"
                },
                new Marca {
                    LocCode = "AC",
                    MarcaId = 2,
                    MarcaName = "MAZDA"
                },
                new Marca {
                    LocCode = "AC",
                    MarcaId = 3,
                    MarcaName = "NISSAN"
                },
                new Marca {
                    LocCode = "AC",
                    MarcaId = 4,
                    MarcaName = "CHINOS"
                },
                 new Marca {
                    LocCode = "AG",
                    MarcaId = 1,
                    MarcaName = "SHIFT"
                },
                new Marca {
                    LocCode = "AG",
                    MarcaId = 2,
                    MarcaName = "MATIZ"
                },
                new Marca {
                    LocCode = "AG",
                    MarcaId = 3,
                    MarcaName = "CHEVROLET"
                },
                new Marca {
                    LocCode = "AG",
                    MarcaId = 4,
                    MarcaName = "CHINOS"
                },
            }.AsQueryable();
        }
    }
}