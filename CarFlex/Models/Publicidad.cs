﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarFlex.Models
{
    public class Publicidad
    {
        public string UrlImg { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }

        public static IQueryable<Publicidad> GetMarcas()
        {
            return new List<Publicidad>
            {
                new Publicidad{
                    UrlImg = "AC",
                    Type = "GARANTÍA CARFLEX",
                    Description = "esCras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit."
                },
                new Publicidad{
                    UrlImg = "AUTO SUSTITUTO",
                    Type = "AUTO SUSTITUTO",
                    Description = "esCras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit."
                },
                new Publicidad{
                    UrlImg = "AC",
                    Type = "FINANCIAMIENTO",
                    Description = "esCras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit."
                },
                new Publicidad{
                    UrlImg = "AC",
                    Type = "TRASLADOS",
                    Description = "esCras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit."
                },
            }.AsQueryable();
        }
    }
}