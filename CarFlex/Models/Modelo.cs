﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarFlex.Models
{
    public class Modelo
    {
        public string Marca { get; set; }
        public string Name { get; set; }
        public string Abreviatura { get; set; }
        public int Status { get; set; }

        public static IQueryable<Modelo> GetModels()
        {
            return new List<Modelo>
            {
                new Modelo {
                    Marca = "1",
                    Name = "R7-571-6858",
                    Abreviatura = "999m",
                    Status = 1
                },
                new Modelo {
                    Marca = "2",
                    Name = "V3-772G-9460",
                    Abreviatura = "912m",
                    Status = 1
                },
                new Modelo {
                    Marca = "2",
                    Name = "V3-571G-9683",
                    Abreviatura = "912m4wo",
                    Status = 1
                },
                new Modelo {
                    Marca = "2",
                    Name = "V3-772G-9653",
                    Abreviatura = "912mrrrs",
                    Status = 1
                },
                new Modelo {
                    Marca = "2",
                    Name = "11-inch MakBook Air",
                    Abreviatura = "912mjuju",
                    Status = 1
                },
                new Modelo {
                    Marca = "3",
                    Name = "13-inch MakBook Air",
                    Abreviatura = "912m32",
                    Status = 1
                },
                new Modelo {
                    Marca = "3",
                    Name = "11-inch MakBook Pro",
                    Abreviatura = "912m33",
                    Status = 1
                },
                new Modelo {
                    Marca = "4",
                    Name = "Fit E15",
                    Abreviatura = "94444",
                    Status = 1
                },

            }.AsQueryable();
        }
    }
}