﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarFlex.Models
{
    public class Localidad
    {

        public string LocCode { get; set; }
        public string LocName { get; set; }

        public static IQueryable<Localidad> GetLocations()
        {
            return new List<Localidad>
            {
                new Localidad {
                    LocCode = "AC",
                    LocName = "MERIDA"
                },
                new Localidad {
                    LocCode = "AL",
                    LocName = "LEON"
                },
                new Localidad {
                    LocCode = "AB",
                    LocName = "CANCUN"
                },
                new Localidad {
                    LocCode = "AG",
                    LocName = "GUADALAJARA"
                },
                new Localidad {
                    LocCode = "AD",
                    LocName = "CIUDAD DE MEXICO"
                },
            }.AsQueryable();
        }
    }
}