﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using Carflex.mx.Flota.Services;
using Carflex.mx.Flota.Model;
using Carflex.mx.Catalogo.Services;
using ST.Patterns.Library.Interfaces;
namespace CarFlex.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            FlotaService cargar = new FlotaService();
            List<SelectListItem> limarcas = new List<SelectListItem>();
            limarcas.Add(new SelectListItem { Text = "Todas", Value = "0" });
            List<SelectListItem> lilocalidades = new List<SelectListItem>();
            lilocalidades.Add(new SelectListItem { Text = "Todas", Value = "0" });

            var localidades = cargar.getAllLocations();
            localidades.ForEach(x => lilocalidades.Add(new SelectListItem { Text = x.Nombre, Value = x.CiudadId.ToString() }));
            
            BusquedaVM busqueda = new BusquedaVM();
            busqueda = cargar.CargaMarcas();
            foreach (var x in busqueda.marcas)
            {
                limarcas.Add(new SelectListItem {
                    Text = x.Nombre,
                    Value = x.MarcaId.ToString(),
                });
            }
            ViewData["lsmarcas"] = limarcas;
            ViewData["lslocalidades"] = lilocalidades;
            ViewData["lsmodelos"] = "Todas";
            ViewData["mva"] = "";
            ViewData["precioinicial"] = 0;
            ViewData["preciofinal"] = 60000;


            return View();
        }
        public ActionResult CargaCombosByMarca(String ID)
        {
           int id = Convert.ToInt32(ID);
           FlotaService cargar = new FlotaService();
            BusquedaVM busqueda = new BusquedaVM();
            var algo = cargar.ModelosByMarcaId(id);
            
                 if (HttpContext.Request.IsAjaxRequest())
                   {
                       return Json(new SelectList(algo.modelos.ToArray(), "Modelo", "Modelo"), JsonRequestBehavior.AllowGet);   
                   }
                   
           return RedirectToAction("Index");
        }
 /*       public ActionResult CargaLocalidadByMarca(String ID)
        {

            int id = Convert.ToInt32(ID);
            FlotaService cargar = new FlotaService();
            List<LocalidadVM> localidades = cargar.getAllLocations();
            var l = localidades.Where(x => x.MarcaId == id);
   if (HttpContext.Request.IsAjaxRequest())
            {
                return Json(new SelectList(l.ToArray(), "LocalidadId", "Nombre"), JsonRequestBehavior.AllowGet);
            }

            return RedirectToAction("Index");
        }   
   */ }
}
