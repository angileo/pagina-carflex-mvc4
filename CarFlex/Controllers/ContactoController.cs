﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarFlex.Models;
using System.Net.Mail;
using CaptchaMvc;

namespace CarFlex.Controllers
{
    public class ContactoController : Controller
    {
        //
        // GET: /Contacto/

        public ActionResult Index()
        {
            ViewBag.Envio = "NoEnvio";
            return View();
        }
        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            db_carflexEntities db = new db_carflexEntities();
            Table_enviocontactos contacto = new Table_enviocontactos();
            contacto.nombre = form["Nombre"];
            contacto.apellido_p = form["Apellido1"];
            contacto.apellido_m = form["Apellido2"];
            // contacto.numero_tel = form["Phone"];
            contacto.numero_tel = form["Lada"] + form["Phone"];
            contacto.tipo_tel = form["tipoTelefono"];
            contacto.email = form["Email"];
            contacto.confirmar_email = form["EmailConfirm"];
            contacto.ciudad_contcto = form["Ciudad"];

            contacto.comentario = form["Direccion"];
            if (form["aviso"] == "exp")
            {
                contacto.tipo_experiencia = true;
                contacto.tipo_sugerenci = false;
                contacto.asunto = form["tipoAsuntoE"];
                contacto.id_oficina = Convert.ToInt32(form["oficinaOrigeninconv"]);
            }
            else
            {
                contacto.id_oficina = Convert.ToInt32(form["oficinaVisito"]);
                contacto.asunto = form["tipoAsuntoS"];
                contacto.tipo_experiencia = false;
                contacto.tipo_sugerenci = true;
            }
            contacto.acepto_politica = true;
            contacto.fecha_envio = DateTime.Now;
            contacto.status_del_envio = false;
            //var nombre = form["asuntoSugerencia"];
            ViewBag.Apellido = form["Agree"];
            ViewBag.Apellido = form["tipoAsuntoS"];
            ViewBag.Apellido = form["tipoAsuntoE"];
            db.AddToTable_enviocontactos(contacto);
            db.SaveChanges();
            SendEmail(contacto.email, contacto.asunto, contacto.nombre, contacto.comentario, contacto.apellido_p, contacto.apellido_m, contacto.numero_tel, contacto.ciudad_contcto);

            ViewBag.Envio = "";
            return View();
        }

        public ActionResult OficeVisitedList()
        {
            db_carflexEntities db = new db_carflexEntities();
            var sucursales = from p in db.Table_sucursal
                             select p;
            //     var localidad = Localidad.GetLocations();

            if (HttpContext.Request.IsAjaxRequest())
            {
                return Json(new SelectList(sucursales.ToArray(), "id_sucurdal", "alias"), JsonRequestBehavior.AllowGet);
            }

            return RedirectToAction("Index");
        }

        public ActionResult ExperienceList()
        {
            db_carflexEntities db = new db_carflexEntities();
            var exp = from p in db.Table_experiencia
                      select p;
            //     var localidad = Localidad.GetLocations();

            if (HttpContext.Request.IsAjaxRequest())
            {
                return Json(new SelectList(exp.ToArray(), "id", "texto"), JsonRequestBehavior.AllowGet);
            }

            return RedirectToAction("Index");
        }

        public ActionResult SugerenceList()
        {
            db_carflexEntities db = new db_carflexEntities();
            var sugiere = from p in db.Table_sugerencia
                          select p;
            //     var localidad = Localidad.GetLocations();

            if (HttpContext.Request.IsAjaxRequest())
            {
                return Json(new SelectList(sugiere.ToArray(), "id", "sugerencia"), JsonRequestBehavior.AllowGet);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ViewResult ContactForm(CarFlex.Models.MailModel _objModelMail)
        {
            if (ModelState.IsValid)
            {
                MailMessage mail = new MailMessage();

                mail.To.Add(_objModelMail.To);
                mail.From = new MailAddress(_objModelMail.From);
                mail.Subject = _objModelMail.Subject;
                string Body = _objModelMail.Body;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential
                ("angelito07@gmail.com", "sofista");// Enter seders User name and password
                smtp.EnableSsl = true;
                smtp.Send(mail);
                return View("Index", _objModelMail);
            }
            else
            {
                return View();
            }
        }

        static void SendEmail(string to, string subject, string name, string coment, string ap1, string ap2, string numtel, string ciud)
        {

            //var from = "";
            //string liga ;
            //liga = "http://www\.avis.mx/Pages/Preguntas/Politica-privacidad-Avis.aspx"; 
            //  string liga2 = "http:\/\/www.avis";
            ciud = "Merida";
            var body = "<html><body><div id=\"bodyreadMessagePartBodyControl393f\" class=\"ExternalClass MsgBodyContainer\" data-link=\"class{:~tag.cssClasses(PlainText, IsContentFiltered)}\"><style>.ExternalClass .ecxstyle1 {width:127px;}.ExternalClass .ecxstyle2 {width:92px;}.ExternalClass .ecxstyle3 {width:127px;height:14px;}</style><table width=\"500\" cellpadding=\"0\" cellspacing=\"0\"><tbody><tr><td width=\"120\" height=\"57\" align=\"left\" valign=\"top\"><img src=\"http://www.avis.mx/Assets/emails/images/logo-avis.gif\" /></td><td width=\"10\" align=\"left\" valign=\"bottom\"></td><td width=\"106\" align=\"left\" valign=\"bottom\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:11px;font-weight:normal;color:#F00;\"></td><td width=\"4\" align=\"left\" valign=\"bottom\"></td><td width=\"228\" align=\"left\" valign=\"middle\" style=\"text-align:right;font-weight:normal;\"><span style=\"font-family:Arial,Helvetica,sans-serif;font-size:16px;color:#F00;\"><b>REGISTRO AVIS</b></span><br></td></tr></tbody></table><table width=\"500\" cellpadding=\"0\" cellspacing=\"0\"><tbody><tr><td height=\"10\" align=\"left\" valign=\"middle\" style=\"text-align:right;font-weight:normal;\"><span style=\"font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#F00;\"><b>ALTA USUARIO</b><br></span></td></tr></tbody></table><table width=\"500\" cellpadding=\"0\" cellspacing=\"0\"><tbody><tr><td height=\"18\" align=\"left\" valign=\"top\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:11px;font-weight:normal;text-align:justify;\"><p>Gracias por utilizar el registro en <b>AVIS.MX</b>; su registro le dará acceso a secciones exclusivas para nuestros Clientes Avis, así como a promociones, no sólo con Avis México sino también con nuestros socios comerciales.<br></p></td></tr></tbody></table><table width=\"500\" cellpadding=\"0\" cellspacing=\"0\" style=\"border:1px solid;border-color:#CCC;border-collapse:collapse;\"><tbody><tr><td height=\"10\"></td><td></td><td height=\"10\"></td></tr><tr><td width=\"24\" height=\"40\" align=\"left\" valign=\"top\"></td><td align=\"left\" valign=\"top\" class=\"ecxstyle2\"><table cellpadding=\"0\" cellspacing=\"0\" style=\"width:426px;\"><tbody><tr><td align=\"left\" valign=\"top\" class=\"ecxstyle3\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:11px;text-align:justify;\" width=\"24\" height=\"24\"><b>Nombre</b></td></tr><tr><td align=\"left\" valign=\"top\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:11px;text-align:justify;\">" + name + " " + ap1 + " " + ap2 + " " + " </td></tr><tr><td align=\"left\" valign=\"top\" class=\"ecxstyle3\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:11px;text-align:justify;\" width=\"24\" height=\"24\"><b>Tel:</b></td></tr><tr><td align=\"left\" valign=\"top\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:11px;text-align:justify;\">" + numtel + " </td></tr><tr><td align=\"left\" valign=\"top\" class=\"ecxstyle1\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:11px;font-weight:normal;text-align:justify;\"><b></b></td></tr><tr><td align=\"left\" valign=\"top\" class=\"ecxstyle3\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:11px;text-align:justify;\"><b>Email</b></td></tr><tr><td align=\"left\" valign=\"top\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:11px;text-align:justify;\"> </td></tr><tr><td align=\"left\" valign=\"top\" class=\"ecxstyle1\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:11px;font-weight:normal;text-align:justify;\"><b></b></td></tr><tr><td align=\"left\" valign=\"top\" class=\"ecxstyle1\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:11px;font-weight:normal;text-align:justify;\"><b>Ciudad: " + ciud + "</b><br></td></tr><tr><td align=\"left\" valign=\"top\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:11px;font-weight:normal;text-align:justify;\"> </td></tr><tr><td align=\"left\" valign=\"top\" class=\"ecxstyle1\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:11px;font-weight:normal;text-align:justify;\"><b></b></td></tr><tr><td align=\"left\" valign=\"top\" class=\"ecxstyle3\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:11px;font-weight:normal;text-align:justify;\"><b>Comentario:" + coment + "</b></td></tr><tr><td align=\"left\" valign=\"top\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:11px;font-weight:normal;\">Para confirmar tu registro haz click en el siguiente link para completar tu informacion</td></tr><tr><td height=\"8\" align=\"left\" valign=\"top\" class=\"ecxstyle1\">&nbsp; </td></tr><tr><td align=\"left\" valign=\"top\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:11px;font-weight:normal;\"><h3></h3></td></tr><tr><td height=\"8\" align=\"left\" valign=\"top\" class=\"ecxstyle1\">&nbsp; </td></tr><tr><td height=\"8\" align=\"left\" valign=\"top\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:10px;font-weight:normal;color:#303030;\">Si tu navegador no habilita la liga, favor de copiarla y pegarla en tu barra de direcciones para continuar con la operación solicitada.</td></tr></tbody></table></td><td width=\"24\" height=\"40\" align=\"left\" valign=\"top\"></td></tr><tr><td height=\"10\"></td><td></td><td height=\"10\"></td></tr></tbody></table><table width=\"500\" cellpadding=\"0\" cellspacing=\"0\" style=\"border:1px solid;border-color:#CCC;border-collapse:collapse;\"><tbody><tr></tr></tbody></table><table width=\"500\" cellpadding=\"0\" cellspacing=\"0\"><tbody><tr><td height=\"8\" align=\"left\" valign=\"top\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:11px;font-weight:normal;color:#303030;\"><p>El tratamiento que AVIS brinda a los datos personales que nos has proporcionado, con tu consentimiento previo, está regulado bajo los principios de licitud, información, calidad, finalidad, lealtad, proporcionalidad y responsabilidad. Permítenos darte a conocer con mayor detalle dicho tratamiento a través de nuestro <a href=" + " target=\"_blank\" style=\"color:#ff0000;font-family:Arial,Helvetica,sans-serif;font-size:11px;\">Aviso de Privacidad</a>, el cual podrá ser consultado en todo momento.</p></td></tr><tr><td height=\"8\"></td></tr></tbody></table><table width=\"500\" cellpadding=\"0\" cellspacing=\"0\"><tbody><tr><td height=\"8\" align=\"center\" valign=\"top\" style=\"font-family:Arial,Helvetica,sans-serif;font-size:11px;font-weight:normal;color:#303030;\"><b><a href=\"https://\" target=\"_blank\" style=\"color:#303030;\">www.avis.mx</a></b><br>© 2011 Avis Mexico </td></tr></tbody></table><br><hr><font face=\"Arial\" color=\"Gray\" size=\"1\"><br>Este mensaje contiene informacion confidencial y esta destinado unicamente para la persona nombrada. Si usted no es el destinatario, no debe difundir, distribuir o copiar este correo. Por favor notifique inmediatamente al remitente por correo si usted ha recibido este por error y borrarlo de su sistema. La transmision de este correo, no se puede garantizar que sea segura o libre de errores o ciertos riesgos, ya que como la informacion puede ser interceptada, corrompida, perdida, destruida, llegar tarde o incompleta, o contener virus. El remitente, por lo tanto, no acepta responsabilidad por cualquier error u omision en el contenido de este mensaje, que surjan como resultado de la transmision del correo electronico. Si la verificacion es necesaria, por favor solicite una version impresa.<br><br>This message contains confidential information and is intended only for the individual named. If you are not the named addressee you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. E-mail transmission cannot be guaranteed to be secure or error-free as information could be intercepted, corrupted, lost, destroyed, arrive late or incomplete, or contain viruses. The sender therefore does not accept liability for any errors or omissions in the contents of this message, which arise as a result of e-mail transmission. If verification is required please request a hard-copy version.<br></font></div></body></html>";

            MailMessage mail = new MailMessage();

            mail.To.Add(to);
            mail.From = new MailAddress("angelito07@gmail.com");
            mail.Subject = subject;
            string Body = body;
            mail.Body = Body;
            mail.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential
            ("angelito07@gmail.com", "sofista");// Enter seders User name and password
            smtp.EnableSsl = true;
            smtp.Send(mail);
        }

    }
}
