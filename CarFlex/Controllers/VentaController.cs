﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Carflex.mx.Flota.Services;
using Carflex.mx.Flota.Model;
using ST.Shared.Interfaces;
using System.Threading.Tasks;
using CarFlex.Controllers;
using Carflex.mx.Catalogo.Services;

namespace CarFlex.Controllers
{
    public class VentaController : Controller
    {   // GET: /Venta/
              
              public ActionResult Index()
              {
                  FlotaService cargar = new FlotaService();
                  List<SelectListItem> limarcas = new List<SelectListItem>();
                  limarcas.Add(new SelectListItem { Text = "Todas", Value = "0" });
                  List<SelectListItem> lilocalidades = new List<SelectListItem>();
                  lilocalidades.Add(new SelectListItem { Text = "Todas", Value = "0" });

                  var localidades = cargar.getAllLocations();
                  localidades.ForEach(x => lilocalidades.Add(new SelectListItem { Text = x.Nombre, Value = x.CiudadId.ToString() }));
                  BusquedaVM busqueda = new BusquedaVM();

                  busqueda = cargar.CargaMarcas();
                  foreach (var x in busqueda.marcas)
                  {
                      limarcas.Add(new SelectListItem
                      {
                          Text = x.Nombre,
                          Value = x.MarcaId.ToString(),
                      });
                  }
                  List<SelectListItem> limodelos = new List<SelectListItem>();
                  limodelos.Add(new SelectListItem { Text = "Todas", Value = "Todas" });

            
                  ViewData["lsmarcas"] = limarcas;
                  ViewData["localidades"] = lilocalidades;
                  ViewData["precio"] = 0.0;
                  ViewData["lsmodelos"] = limodelos;
                  ViewData["mva"] = "";
                  ViewData["modelomostrar"] = "Todas";
                  var lsautos = cargar.BuscarAutos(0, 0, "Todas", "", 0.0, 0.0);
                  ViewData["passedArray"] = lsautos;
                  return View();
              }

            
       [HttpPost]
       public ActionResult Index(FormCollection form)
        {
            Carflex.mx.Flota.Services.FlotaService cargar = new Carflex.mx.Flota.Services.FlotaService();

            int marcaid = 0;
            string modelo = "Todas";
            int localidadid = 0;
            string mva = "";
            ViewData["mva"] = mva;
            double precioInicial =0.0;
            if (!string.IsNullOrEmpty(form["mva"]))
            {
                mva = (form["mva"]);
                ViewData["mva"] = mva;
            }

            List<SelectListItem> lilocalidades = new List<SelectListItem>();
            lilocalidades.Add(new SelectListItem { Text = "Todas", Value = "0" });          
            if ((form["localidades"]) == "0")
            {
                localidadid = Convert.ToInt32(form["localidades"]);
                LocalidadVM locs = new LocalidadVM();
                var localidades = cargar.getAllLocations();
                foreach (var x in localidades)
                {
                    lilocalidades.Add(new SelectListItem
                    {
                        Text = x.Nombre,
                        Value = x.CiudadId.ToString()
                    });
                }
                ViewData["localidades"] = lilocalidades;
            }
            else
            {
                localidadid = Convert.ToInt32(form["localidades"]); //un entero mayor que cero
                LocalidadVM locs = new LocalidadVM();
                var localidades = cargar.getAllLocations();
                foreach (var x in localidades)
                {
                    lilocalidades.Add(new SelectListItem
                    {
                        Text = x.Nombre,
                        Value = x.CiudadId.ToString(),
                        Selected = localidadid == x.CiudadId ? true : false
                    });
                }
                ViewData["localidades"] = lilocalidades;
                var localidadmostrar = from s in lilocalidades where s.Selected select s.Text;
                string localidad2 = localidadmostrar.FirstOrDefault();
                ViewData["localidadmostrar"] = localidad2;
            }

            List<SelectListItem> limarcas = new List<SelectListItem>();
            limarcas.Add(new SelectListItem { Text = "Todas", Value = "0" });
            if ((form["marcas"]) == "0")
            {
                ViewData["marcamostrar"] = null;
                marcaid = Convert.ToInt32(form["marcas"]);
                BusquedaVM busqueda = new BusquedaVM();
                busqueda = cargar.CargaMarcas();
                foreach (var x in busqueda.marcas)
                {
                    limarcas.Add(new SelectListItem
                    {
                        Text = x.Nombre,
                        Value = x.MarcaId.ToString()
                    });
                }
                ViewData["lsmarcas"] = limarcas;
                List<SelectListItem> limodelos = new List<SelectListItem>();
                limodelos.Add(new SelectListItem { Text = "Todas", Value = "Todas" });
                ViewData["lsmodelos"] = limodelos;
            }
            else
            {
                ViewData["marcamostrar"] = (form["marcas"]);
                marcaid = Convert.ToInt32(form["marcas"]); //recibe un entero mayor que cero
                BusquedaVM busqueda = new BusquedaVM();
                busqueda = cargar.CargaMarcas();
                foreach (var x in busqueda.marcas)
                {
                    limarcas.Add(new SelectListItem
                    {
                        Text = x.Nombre,
                        Value = x.MarcaId.ToString(),
                        Selected = marcaid == x.MarcaId ? true : false
                    });
                }
                ViewData["lsmarcas"] = limarcas;
                string marcamostrar = busqueda.marcas.Where(m => m.MarcaId.Equals(marcaid)).Select(d => d.Nombre).FirstOrDefault();
                ViewData["marcamostrar"] = marcamostrar;

                List<SelectListItem> limodelos = new List<SelectListItem>();
                limodelos.Add(new SelectListItem { Text = "Todas", Value = "Todas" });
                if ((form["modelos"]) != "Todas")
                {
                    modelo = (form["modelos"]);
                    var lsmarca = cargar.ModelosByMarcaId(marcaid);

                    foreach (var x in lsmarca.modelos)
                    {
                        limodelos.Add(new SelectListItem
                        {
                            Text = x.Modelo,
                            Value = x.Modelo,
                            Selected = modelo == x.Modelo ? true : false
                        });
                    }
                   
                    ViewData["modelomostrar"] = modelo;
                    ViewData["lsmodelos"] = limodelos;
                }
                else {
                    modelo = (form["modelos"]);//debe ser "Todas"
                var lsmarca = cargar.ModelosByMarcaId(marcaid);
                foreach (var x in lsmarca.modelos)
                {
                    limodelos.Add(new SelectListItem
                    {
                        Text = x.Modelo,
                        Value = x.Modelo
                    });
                }
                ViewData["lsmodelos"] = limodelos;
                ViewData["modelomostrar"] = "Todas";
                }
            }
           if (form["slider-snap-value-lower1"].Trim()!="" && !form["slider-snap-value-lower1"].Trim().Equals("0"))
            {
                double precio = Convert.ToDouble(form["slider-snap-value-lower1"]);
                ViewData["precio"] = precio;
                precioInicial = precio;
            }
            else {
                
                ViewData["precio"] = precioInicial;
            }

            double precioFinal = Convert.ToDouble(form["slider-snap-value-upper1"]); 
          
                var lsautos = cargar.BuscarAutos(marcaid, localidadid, modelo, mva, precioInicial, precioFinal);
                ViewData["passedArray"] = lsautos;
           return View();
        }
       [HttpPost]
       public ActionResult BuscarAutoAjax(FormCollection form)
       {
           Carflex.mx.Flota.Services.FlotaService cargar = new Carflex.mx.Flota.Services.FlotaService();

           int marcaid = 0;
           string modelo = "Todas";
           int localidadid = 0;
           string mva = "";
           double precioInicial = 0.0;
           if (!string.IsNullOrEmpty(form["mva"]))
           {
               mva = (form["mva"]);
           }

           if ((form["localidades"]) != "0")
           {
               localidadid = Convert.ToInt32(form["localidades"]);
           }

           if ((form["marcas"]) != "0")
           {
               marcaid = Convert.ToInt32(form["marcas"]);
               if ((form["modelos"]) != "Todas")
               {
                   modelo = (form["modelos"]);
               }
           }
           if (form["slider-snap-value-lower1"].Trim() != "" && !form["slider-snap-value-lower1"].Trim().Equals("0"))
           {
               precioInicial = Convert.ToDouble(form["slider-snap-value-lower1"]);
           }

           double precioFinal = Convert.ToDouble(form["slider-snap-value-upper1"]);

           var lsautos = cargar.BuscarAutos(marcaid, localidadid, modelo, mva, precioInicial, precioFinal);
           Dictionary<string, object> dic = new Dictionary<string, object>();
           dic.Add("fondo", lsautos);
           dic.Add("tugarganta", "amplia");
           return Json(lsautos);
       }
       public ActionResult CargaCombosByMarca(String ID)
        {

            int id = Convert.ToInt32(ID);
            FlotaService cargar = new FlotaService();
            BusquedaVM busqueda = new BusquedaVM();
            var algo1 = cargar.ModelosByMarcaId(id);
            var algo = algo1.modelos.GroupBy(r => r.Modelo);
            if (HttpContext.Request.IsAjaxRequest())
            {
                return Json(new SelectList(algo.ToArray(), "Key", "Key"), JsonRequestBehavior.AllowGet);
            }

            return RedirectToAction("Index");
        }
    /*   public ActionResult CargaLocalidadByMarca(String ID)
        {

            int id = Convert.ToInt32(ID);
            FlotaService cargar = new FlotaService();
            List<LocalidadVM> localidades = cargar.getAllLocations();
            var l = localidades.Where(x => x.MarcaId == id);
            if (HttpContext.Request.IsAjaxRequest())
            {
                return Json(new SelectList(l.ToArray(), "LocalidadId", "Nombre"), JsonRequestBehavior.AllowGet);
            }

            return RedirectToAction("Index");
        }
    */  
        public ActionResult DetalleVehiculo(int id)
       {
           int i = 6;
           i = i + 1;
           return View();
       }
        public ActionResult detalleporid(int id)
        {
            string algo = "cdcd";
            ViewBag.algo = algo;
            return PartialView(ViewBag.algo);
        }
    }
}
