﻿$(document).ready(function () {
    flotantes();
    $('#LocalidadesID').change(function () {
        flotantes();
        var formData = $("#formulario").serializeArray();
        var URL = 'venta/BuscarAutoAjax/';
        var tablaDatos = $("#tblDatos");
        $.post(URL,
            formData,
            function (data, textStatus, jqXHR) {
                tablaDatos.empty();
                for (i = 0; i <= data.length; i++) {                   
                    $("#tblDatos").append("<tr><td>" + data[i].autoimagenid + "</td><td>" + data[i].version + "</td><td>" + data[i].modelo + "</td><td>" + data[i].ciudadid + "</td><td>" + data[i].MVA + "</td><td>" + data[i].precio + "</td></tr>");
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {

            });
    });
    $('#ModelosID').change(function () {
        flotantes();
        var formData = $("#formulario").serializeArray();
        var URL = 'venta/BuscarAutoAjax/';
        var tablaDatos = $("#tblDatos");
        $.post(URL,
            formData,
            function (data, textStatus, jqXHR) {
                tablaDatos.empty();
                //alert(data.MVA[1]);
             //   S('#marcamostrar').Text(data.tugarganta);
                for (i = 0; i <= data.length; i++) {
                    tablaDatos.append("<tr><td>" + data[i].autoimagenid + "</td><td>" + data[i].version + "</td><td>" + data[i].modelo + "</td><td>" + data[i].ciudadid + "</td><td>" + data[i].MVA + "</td><td>" + data[i].precio + "</td></tr>");
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {

            });
    });
    $('#MarcasID').change(function () {
        flotantes();
        var URLmodelos = 'venta/CargaCombosByMarca/' + $('#MarcasID').val();
        $.getJSON(URLmodelos, function (data) {
            var items = '<option value="Todas">Todas</option>';
            $.each(data, function (i, category) {
                tablaDatos.empty();
                items += "<option value='" + category.Value + "'>" + category.Text + "</option>";
            });
            $('#ModelosID').html(items);
        });

        var formData = $("#formulario").serializeArray();
        var tablaDatos = $("#tblDatos");
        var URL = 'venta/BuscarAutoAjax/';
        $.post(URL,
            formData,
            function (data, textStatus, jqXHR) {
                tablaDatos.empty();
               // alert(data[0].version)
                console.log(data[7].autoimagenid);
                for (i = 0; i <= data.length; i++)
                {
                    
                    tablaDatos.append("<tr><td>" + data[i].autoimagenid + "</td><td>" + data[i].version + "</td><td>" + data[i].modelo + "</td><td>" + data[i].ciudadid + "</td><td>" + data[i].MVA + "</td><td>" + data[i].precio + "</td></tr>");
                }
                //data: Data from server.    
            }).fail(function (jqXHR, textStatus, errorThrown) {

            });
    });
    function flotantes() {
        localidadmostrar = $('#LocalidadesID option:selected').text();
        if (localidadmostrar != "Todas") {
            document.getElementById('localidadmostrar').style.display = 'block';
            $('#localidadmostrar').text(localidadmostrar);
        }
        else {
            $('#localidadmostrar').hide();
        }
        modelomostrar = $('#ModelosID option:selected').text();
        if (modelomostrar != "Todas"){
            document.getElementById('modelomostrar').style.display = 'block';
            $('#modelomostrar').text(modelomostrar);
        }
        else { 
            $('#modelomostrar').hide();
        }
        marcamostrar = $('#MarcasID option:selected').text();
        if (marcamostrar != "Todas") {
            document.getElementById('marcamostrar').style.display = 'block';
            $('#marcamostrar').text(marcamostrar);
        }
        else {
            $('#marcamostrar').hide();
        }
    };
    $(".listaflotante a").click(function () {
      //  alert('Soy ' + $(this).attr("id") + ' y mi texto es: ' + $(this).text());
        $(this).hide();
        if ($(this).attr("id") == "marcamostrar"){
        $('#ModelosID').prop('selectedIndex', 0);
        $('#MarcasID').prop('selectedIndex', 0);
        }
        if ($(this).attr("id") == "localidadmostrar")
            $('#LocalidadesID').prop('selectedIndex', 0);
        if ($(this).attr("id") == "modelomostrar")
            $('#ModelosID').prop('selectedIndex', 0);
        var formData = $("#formulario").serializeArray();
        var tablaDatos = $("#tblDatos");
        var URL = 'venta/BuscarAutoAjax/';
        $.post(URL,
            formData,
            function (data, textStatus, jqXHR) {
                tablaDatos.empty();
                // alert(data[0].version)
                console.log(data[7].autoimagenid);
                for (i = 0; i <= data.length; i++) {

                    tablaDatos.append("<tr><td>" + data[i].autoimagenid + "</td><td>" + data[i].version + "</td><td>" + data[i].modelo + "</td><td>" + data[i].ciudadid + "</td><td>" + data[i].MVA + "</td><td>" + data[i].precio + "</td></tr>");
                }
                //data: Data from server.    
            }).fail(function (jqXHR, textStatus, errorThrown) {

            });
    });

});