// JavaScript Document
$(document).ready(function () {
    if ($(window).width() <= 991) {
        $('.rangoPrecioForm').appendTo('#traerForm');
    } else if ($(window).width() >= 992) {
        $('.rangoPrecioForm').appendTo('#regresarForm');
    }
    $(window).bind('resize', function () {
        if ($(window).width() <= 991) {
            $('.rangoPrecioForm').appendTo('#traerForm');
        } else if ($(window).width() >= 992) {
            $('.rangoPrecioForm').appendTo('#regresarForm');
        }
    });
});

$(function() {
	var pop = $('.popbtn');
	var row = $('.row:not(:first):not(:last)');


	pop.popover({
		trigger: 'manual',
		html: true,
		container: 'body',
		placement: 'bottom',
		animation: false,
		content: function() {
			return $('#popover').html();
		}
	});


	pop.on('click', function(e) {
		pop.popover('toggle');
		pop.not(this).popover('hide');
	});

	$(window).on('resize', function() {
		pop.popover('hide');
	});

	row.on('touchend', function(e) {
		$(this).find('.popbtn').popover('toggle');
		row.not(this).find('.popbtn').popover('hide');
		return false;
	});

});

/*..::::::::::========== SCRIPT PARA CARGAR AUTOMATICAMENTE LA VENTANA MODAL ==========::::::::::..*/
window.onload = function() {
	$('#myModal').modal('show')
}

/*..::::::::::========== SLIDER ==========::::::::::..*/
$('.carousel').carousel({
  interval: 5000
})

$('#myCarousel').swipe( {
    swipeLeft: function() {
        $(this).carousel('next');
    },
    swipeRight: function() {
        $(this).carousel('prev');
    },
    allowPageScroll: 'vertical'
});

/*..::::::::::========== SLIDER PARA EL RANGO DE PRECIOS DEL FORMULARIO DEL HOME ==========::::::::::..
$('#slider-snap').noUiSlider({
            start: [ 20000, 150000 ],
            connect: true,
            range: {
                'min': 0,
                '10%': 15000,
                '20%': 25000,
                '30%': 30000,
                '40%': 75000,
                '50%': 150000,
                'max': 300000
            }
        });
        $('#slider-snap').Link('lower').to($('#slider-snap-value-lower'));

        $('#slider-snap').Link('upper').to($('#slider-snap-value-upper'));

*/

/*..::::::::::========== SCRIPT PARA CAMBIAR POSICIÓN DEL FORMULARIO DEL HOME ==========::::::::::..*/
/*$(document).load($(window).bind('resize', checkResolucion));
    function checkResolucion()
    {
        if($(window).width() <= 991)
        {
            $('.rangoPrecioForm').appendTo('#traerForm');
        } else if($(window).width() >= 992){
            $('.rangoPrecioForm').appendTo('#regresarForm');
        }
    }*/


/*$(document).ready(function() {
    if (screen.width<=991) 
    {
        $('.rangoPrecioForm').appendTo('#traerForm');
    } else if($(screen.width>= 992){
        $('.rangoPrecioForm').appendTo('#regresarForm');
    }
});*/

/*..::::::::::========== VALIDAR FORMULARIO DE CONTACTO ==========::::::::::..*/

