﻿$(document).ready(function () {
    var URL = 'Contacto/OficeVisitedList/';
    $.getJSON(URL, function (data) {
        var items = '<option value="">Oficina Carflex que visitó</option>';
        $.each(data, function (i, manufacturer) {
            items += "<option value='" + manufacturer.Value + "'>" + manufacturer.Text + "</option>";
        });
        $('#oficinaVisito').html(items);
        $('#oficinaOrigeninconv').html(items);
    });

    var URL1 = 'Contacto/SugerenceList/';
    $.getJSON(URL1, function (data) {
        var items = '<option value="">Seleccione sugerencia</option>';
        $.each(data, function (i, manufacturer) {
            items += "<option value='" + manufacturer.Value + "'>" + manufacturer.Text + "</option>";
        });
        $('#asuntoSugerencia').html(items);
    });

    var URL2 = 'Contacto/ExperienceList/';
    $.getJSON(URL2, function (data) {
        var items = '<option value="">Seleccione experiencia</option>';
        $.each(data, function (i, manufacturer) {
            items += "<option value='" + manufacturer.Value + "'>" + manufacturer.Text + "</option>";
        });
        $('#asuntoExperiencia').html(items);
    });
});